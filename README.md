# Desafio Fullstack CDV #

Para resolução do desafio optei por utilizar a linguagem PHP com CodeIgniter versão 3. O SGBD utilizado foi o MySQL. Para o frontend utilizei JQuery e Bootstrap 4.

Para instalação do projeto basta utilizar um servidor linux ou windows com PHP, MySQL e Apache instalados. A versão do PHP mais recente funcionará sem problemas.

Deve-se habilitar o mod_rewrite do Apache.

Na raiz do projeto está o arquivo "cdv.sql" com o script para criação do banco de dados.

Deve-se alterar o arquivo application/config/config.php na linha 26 e informar a url base onde o projeto foi instalado. Todos os links da aplicação fazem referência a essa url base.

```bash
$config['base_url'] = 'informar URL base para o projeto';
```

Para informar as credenciais do banco de dados, altere o arquivo application/config/database.php.

## Versão Online
Para facilitar a análise da aplicação, deixei uma versão disponível em uma instância da AWS. O endereço é: http://54.94.8.137/cdv/

As credenciais do usuário teste são:

* e-mail: john.doe@example.com
* senha: cdv123

## O que faltou fazer?

A parte dos testes de código ficou em falta. O desenvolvimento orientado a testes ainda é uma habilidade que preciso exercitar.

Por falta de tempo não consegui gerar uma imagem do Docker com a aplicação instalada, como sugeria o desafio. Optei pela versão online, conforme já exposto.

Também por falta de tempo, não fiz o carregamento assíncrono dos dados nas tabelas. Em cenários com muitos registros seria interessante que os dados exebidos fossem carregados conforme a demanda.

## Atendimento aos requisitos

A aplicação permite: 
* Cadastrar, listar, atualizar e remover clientes. 
* Visualizar o portfólio de um cliente específico, calculando suas posições.
* Visualizar as posições anteriores de um cliente com base em um período selecionado pelo usuário.
* Permite listar e remover movimentações. 
* Permite cadastrar movimentações em lote. Pensando em escala, optei pelo processamento assíncrono do arquivo de lote. O processamento é acionado por uma URL e poderia ser executado como uma tarefa agendada no próprio servidor da aplicação ou em outro servidor. O usuário pode acompanhar o status do processamento do arquivo de lote em seção específica dentro da aplicação.
* Um dos requisitos do desafio era prover acesso de API para que fosse possível consultar todas as posições de um ou mais clientes. Isso é possível através de uma solicitação GET enviada para o endpoint:

```bash
http://54.94.8.137/cdv/clientes/portfolios/?id=1,2,3,4
```

Observe que os ids dos clientes são passados no parâmetro "id" e são separados por vírgula. 

Para simular o processamento assíncrono do arquivo de lote para cadastro das movimentações, execute a URL:

```bash
http://54.94.8.137/cdv/lotes/processar
```

## Créditos

* Fiz uso do template Tabler, disponível em: https://tabler.io/
* Utilizei o plugin DataTable, disponível em: https://datatables.net/

