<?php defined('BASEPATH') or exit('No direct script access allowed');

class Clientes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Carrega o model para acesso ao banco e renomeia o objeto para 'con'
        $this->load->model('persistent', 'con'); 
    }

    /**
     * Action default
     */
    public function index()
    {
        //Verifica se existe um usuário logado para exbir a página
        if (is_null($this->session->userdata('uid'))) {
            redirect(base_url('login'));
        }
        
        //Busca todos os clientes
        $clientes = $this->con->select('cliente');

        //Variáveis que serão enviadas para view
        $data = array(
            'page_title' => 'Clientes',
            'clientes' => $clientes
        );

        //Renderiza a view
        $this->load->view('header', $data);
        $this->load->view('pages/clientes', $data);
        $this->load->view('footer');
    }

    /**
     * Action save é acionada via ajax. Recebe parâmetros via POST
     * e insere um cliente na base de dados.
     */
    public function save(){
        $id = $this->input->post('id');
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $infoAdicional = $this->input->post('info-adicional');

        //Variáveis para checagem da conclusão das operações
        $insert = null;
        $update = null;

        if(empty($id)){
            //Inserção
            $insert = $this->con->insert('cliente', array(
                'nome' => $nome,
                'email' => $email,
                'info-adicional' => $infoAdicional
            ));
        }else{
            //Update
            $update = $this->con->update('cliente', array(
                'nome' => $nome,
                'email' => $email,
                'info-adicional' => $infoAdicional
            ), array('id' => $id));
        }

        if($insert || $update){
            //As chamadas ajax sempre devolvem um json que será lido pelo JavaScript no front-end da aplicação
            echo json_encode(array('success' => true));
        }
    }

    /**
     * Acionada via ajax. Com base no id de um cliente, exibe um json
     * contendo suas informações.
     */
    public function get($id){
        if(!empty($id)){
            $cliente = $this->con->select('cliente', '*', array('id' => $id));
            if(isset($cliente[0])){
                echo json_encode(array(
                    'success' => true,
                    'data' => $cliente[0]
                ));
            }else{
                echo json_encode(array(
                    'error' => true
                ));
            }
        }
    }

    /**
     * Acionada via ajax. Com base no id de um cliente, remove seu registro da base de dados.
     */
    public function delete($id){
        if(!empty($id)){
            $status = $this->con->delete('cliente', array('id' => $id));
            if($status){
                echo json_encode(array(
                    'success' => true
                ));
            }else{
                echo json_encode(array(
                    'error' => true
                ));
            }
        }
    }

    /**
     * Acionado via ajax. Retorna as posições de um cliente com base em seu id.
     * Podem ser informados parâmetros de datas para busca por períodos específicos.
     */
    public function posicoes($id){

        //As datas enviadas devem estar no formtado YYYY-mm-dd
        $de = $this->input->post('de');
        $ate = $this->input->post('ate');

        $posicoes = $this->findPosicoes($id, $de, $ate);

        echo json_encode(array(
            'success' => true,
            'posicoes' => $posicoes
        ));
    }

    /**
     * Método usado internamente por outros métodos.
     * Dado o $id de um cliente, retorna todas as suas posições.
     * Podem ser informados parâmetros de datas para busca por períodos específicos.
     */
    private function findPosicoes($id, $de = null, $ate = null){

        $posicoes = array();//Para agrupar as posições que serão retornadas

        //Busco as movimentações do cliente
        if(empty($de) || empty($ate)){
            $movimentacoes = $this->con->select('movimentacao', '*', array('cliente' => $id));
        }else{
            //Não deu tempo de usar uma abordagem mais ORM (objeto-relacional)
            //Busca todas as movimentacoes entre um período de datas
            $movimentacoes = $this->con->query("SELECT * FROM `tab_movimentacao` WHERE data BETWEEN '{$de}' and '{$ate}'");
        }
        
        if($movimentacoes){
            //A primeira iteração apenas acumula as quantidades e os totais envolvidos para cada ativo
            foreach($movimentacoes as $movimentacao){

                //Uma verificação necessária para agrupar as posições por ativos
                if(!isset($posicoes[$movimentacao['ativo']])){
                    $posicoes[$movimentacao['ativo']] = array(
                        'ativo' => $movimentacao['ativo'],
                        'qtd' => 0,
                        'acum' => 0, //Para acumular (qtd * valor)
                        'total' => 0 //Para acumular as quantidades
                    );
                }

                //Movimentações do tipo 1: Compras
                //Movimentações do tipo 2: Vendas

                if($movimentacao['tipo'] == '1'){
                    //Compra
                    $posicoes[$movimentacao['ativo']]['qtd'] += $movimentacao['qtd'];
                    $posicoes[$movimentacao['ativo']]['acum'] += (floatval($movimentacao['qtd']) * floatval($movimentacao['valor']));
                    $posicoes[$movimentacao['ativo']]['total'] += $movimentacao['qtd'];
                }else{
                    //Venda
                    $posicoes[$movimentacao['ativo']]['qtd'] -= $movimentacao['qtd'];
                    if($posicoes[$movimentacao['ativo']]['qtd'] < 0){
                        $posicoes[$movimentacao['ativo']]['qtd'] = 0;
                    }
                }
            }
        }
        return $posicoes;
    }

    /**
     * Para retornar as opsições de um ou mais clientes.
     * O objetivo é atender o requisito do desafio que espera esse tipo de chamada de API.
     */
    public function portfolios(){
        $portfolios = array();

        //O parâmetro 'id' conterá os ids separados por vírgula, conforme o exemplo:
        // ?id=203,4,565,67
        $ids = $this->input->get('id');

        if($ids){
            //Obtenho os ids dos clientes
            $client_ids = explode(',', $ids);
            if(count($client_ids) > 0){
                foreach($client_ids as $client){
                    $portfolios[trim($client)] = $this->findPosicoes(trim($client));
                }
            }
        }

        //Calcula o Preço Médio (PM) de aquisição de cada ativo
        if(!empty($portfolios)){
            foreach($portfolios as $k => $ativo){
                foreach($ativo as $key => $v){
                    $v['pm'] = number_format($v['acum']/$v['total'], 2);
                    unset($v['acum']);
                    unset($v['total']);
                    $portfolios[$k][$key] = $v;
                }
            }
        }

        echo json_encode($portfolios);
    }
}
