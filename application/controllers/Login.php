<?php defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!is_null($this->session->userdata('uid'))) {
            redirect(base_url());
        }

        $this->load->view('pages/login');
    }

    /**
     * Acionada via ajax. Retorna um json informando se a operação de login foi ou não bem-sucedida.
     */
    public function entrar()
    {
        //Carrega o model para acesso ao banco e renomeia o objeto para 'con'
        $this->load->model('persistent', 'con');

        $email = ($this->input->post('email'));
        $senha = ($this->input->post('senha'));

        $login = $this->con->select('usuario', '*', array('email' => $email, 'senha' => md5($senha)));

        if (empty($login)) {
            echo json_encode(
                array('error' => 'Email ou senha inválidos!')
            );
        
        } else {
            $this->session->set_userdata(
                array(
                    'uid' => $login[0]['id']
                )
            );

            //As funções que atendem chamadas ajax sempre retornam um json com parâmetros para uso do front-end
            echo json_encode(
                array('success' => 'true')
            );
        }
    }

    /**
     * Realiza a operação de logout
     */
    public function sair()
    {
        $this->session->sess_destroy();
        echo json_encode(
            array('success' => 'true')
        );
    }
}
