<?php defined('BASEPATH') or exit('No direct script access allowed');

class Lotes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Carrega o model para acesso ao banco e renomeia o objeto para 'con'
        $this->load->model('persistent', 'con');
    }

    public function index()
    {
        //Verifica se existe um usuário logado para exbir a página
        if (is_null($this->session->userdata('uid'))) {
            redirect(base_url('login'));
        }
        
        //Seleciona todos os registros de lote
        $lotes = $this->con->select('lote');

        //Variáveis que serão enviadas para view
        $data = array(
            'page_title' => 'Cadastro em Lote de Movimentações',
            'lotes' => $lotes
        );

        //Renderiza a view
        $this->load->view('header', $data);
        $this->load->view('pages/lotes', $data);
        $this->load->view('footer');
    }

    /**
     * Este método recebe e trata o arquivo para cadastro em lote das movimentações.
     */
    public function upload(){

        //Validações para o arquivo de lote
        if (isset($_FILES['lote']['size']) && ($_FILES['lote']['size'] > 0)) {
            //$config = array();
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = 10000;
            $config['overwrite'] = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('lote')) {
                echo json_encode(array('error' => $this->upload->display_errors()));
            } else {
                $lote = $this->upload->data();

                $filename = date('dmYHms');

                //Salvo no diretório onde ficam os arquivos de lote
                copy($lote['full_path'], "./uploads/{$filename}{$lote['file_ext']}");
                unlink($lote['full_path']);

                try{
                    //Salvo as informações do arquivo no banco de dados para processamento posterior
                    $insert = $this->con->insert('lote', array(
                        'filename' => $lote['file_name'],
                        'internal_filename' => "{$filename}.csv",
                        'data' => date('Y-m-d H:m:s'),
                        'tamanho' => $lote['file_size'],
                        'status' => 1
                    ));

                    if($insert){
                        echo json_encode(array('success' => true));
                    }else{
                        echo json_encode(array("error" => "Houston, we have a problem!"));
                    }

                }catch(Exception $e){
                    echo json_encode(array("error" => $e->getMessage()));
                }
            }
            
        } else {
            echo json_encode(array("error" => "Problemas com o arquivo enviado. Você deve enviar um arquivo CSV."));
        }
    }

    /**
     * Método para uso interno. Recebe o caminho de um arquivo CSV e faz o parser dele.
     */
    private function cvsParse($path) 
    {
        if (($handle = fopen($path, "r")) !== FALSE) {
            $fields = fgetcsv($handle, 1000, ",");
            $num = count($fields);
            for ($i=0; $i < $num; $i++) {
                    $index[$i] =  $fields[$i];
            }

            $i = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $content[$i] = array();
                if( $data != null ) { // skip empty lines
                    if(count($index) == count($data)) {
                        $num = count($data);
                        for ($c=0; $c < $num; $c++) {
                            $content[$i][$index[$c]] = $data[$c];
                        }
                    }
                }
                $i++;
            }
            fclose($handle);
        }
        return $content;
    }

    /**
     * Este método busca e processa todos os arquivos de lote ainda não processados.
     * Usa transaction para garantir que eventuais problemas no processamento possam ser revertidos.
     * Deve ser acionado por uma rotina assíncrona.
     */
    public function processaFila(){
        echo 'Aguarde ... <br>';
        //Busco as informações dos lotes que ainda não foram processados
        $lotes = $this->con->select('lote', array('id', 'internal_filename'), array('status' => 1));
        if($lotes){

            foreach($lotes as $lote){
                $movimentacoes = $this->cvsParse("./uploads/{$lote['internal_filename']}");//path to csv file
                
                //informe que o lote está em processamento
                $this->con->update('lote', array('status' => 2), array('id' => $lote['id']));
                
                //Usado para armazenar o id dos clientes já identificados.
                $clientes = array();

                /* O processamento do arquivo de lote será feito por meio de transaction.
                 * Se ocorrerem erros o processamento será abortado e o lote ficará com status de erro.
                 */
                $this->db->trans_begin();

                foreach($movimentacoes as $movimentacao){
                    // Verifico se o cliente informado existe
                    if(!isset($clientes[$movimentacao['Cliente']])){
                        //Busca-se o id do cliente. A tabela de movimentações armazena o id e não o nome do cliente associado.
                        $cliente = $this->con->select('cliente', array('id'), array('nome' => trim($movimentacao['Cliente'])));
                        if($cliente){
                            /* Armazeno o id do cliente em array específico para utilizá-ló logo mais adiante.
                             * Na próxima movmentação do mesmo cliente, a busca no banco não será necessária.
                             */
                            $clientes[$movimentacao['Cliente']] = $cliente[0]['id'];
                        }else{
                            //Se o cliente não existir igonoro a movimentação.
                            break;
                        }
                    }

                    try{
                        $this->con->insert('movimentacao', array(
                            'data' => date('Y-m-d', strtotime(date('d/m/y', strtotime( $movimentacao['Data'])))),
                            'cliente' => $clientes[$movimentacao['Cliente']],
                            'ativo' => $movimentacao['Ativo'],
                            'tipo' => (trim($movimentacao['Tipo']) === 'Compra') ? 1 : 2,
                            'qtd' => $movimentacao['Quantidade'],
                            'valor' => floatval(str_replace(',', '.', $movimentacao['Valor Unitário']))
                        ));
                    }catch(Exception $e){}
                }

                //Finalizo a transação
                $this->db->trans_complete();

                if ($this->db->trans_status() === true) {
                    //Deu tudo certo
                    $this->db->trans_commit();
                    //Apago o arquivo de lote já processado
                    unlink("./uploads/{$lote['internal_filename']}");
                    //Informo que o lote foi processado com sucesso
                    $this->con->update('lote', array('status' => 3), array('id' => $lote['id']));
                }else{
                    // Problemas no processamento deste lote
                    $this->db->trans_rollback();
                    //Informo que o lote foi processado com erro
                    $this->con->update('lote', array('status' => 4), array('id' => $lote['id']));
                }
            }
        }
        echo 'Processamento concluído!';
    }
}
