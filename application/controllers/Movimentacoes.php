<?php defined('BASEPATH') or exit('No direct script access allowed');

class Movimentacoes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Carrega o model para acesso ao banco e renomeia o objeto para 'con'
        $this->load->model('persistent', 'con');
    }

    public function index()
    {
        //Verifica se existe um usuário logado para exbir a página
        if (is_null($this->session->userdata('uid'))) {
            redirect(base_url('login'));
        }
        
        /* Busca todas as movimentações
         * Não deu tempo de fazer uma abordagem mais ORM (objeto-relacional)
         * Faz um join entre a tabela de movimentações e a tabela cliente para capturar os nomes dos clientes
         * os clientes são referenciados na tabela de movimentações apenas por seu id
         */
        $movimentacoes = $this->con->query("SELECT m.*, c.nome from tab_movimentacao m INNER JOIN tab_cliente c on m.cliente = c.id");

        //Variáveis que serão enviadas para view
        $data = array(
            'page_title' => 'Movimentações',
            'movimentacoes' => $movimentacoes
        );

        //Renderiza a view
        $this->load->view('header', $data);
        $this->load->view('pages/movimentacoes', $data);
        $this->load->view('footer');
    }

    /**
     * Acionada via ajax. Remove o registro de uma movimentação com base no seu id.
     */
    public function delete($id){
        if(!empty($id)){
            $status = $this->con->delete('movimentacao', array('id' => $id));
            if($status){
                echo json_encode(array(
                    'success' => true
                ));
            }else{
                //Todas as operações que atendem solicitações ajax devolvem um json com parâmetros para controle do front-end
                echo json_encode(array(
                    'error' => "Não foi possível concluir a operação. Tente novamente em instantes."
                ));
            }
        }
    }
}
