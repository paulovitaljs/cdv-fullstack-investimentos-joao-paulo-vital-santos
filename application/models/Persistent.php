<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Esta classe contém operações de CRUD que podem ser usadas para qualquer tabela
 * desde que seja definido o nome da tabela no momento da operação. É possível usar um prefixo
 * para o nome da tabela, bastante informar no construtor do objeto.
 * Os nomes dos métodos são sugestivos das operações que eles realizam.
 */
class Persistent extends CI_Model{

    private $prefix = '';

    public function __construct($prefix = 'tab_') {
        parent::__construct();
        $this->prefix = $prefix;
        $this->load->database();
    }

    public function setTablePrefix($prefix){
        $this->prefix = $prefix;
    }

    public function select($table, $fields = '*', $conditions = null, $object = false, $orderBy = null){
        $this->db->select($fields);
        $this->db->from($this->prefix.$table);
        if(!is_null($conditions)){
            $this->db->where($conditions);
        }
        if($orderBy){
            $this->db->order_by($orderBy);
        }
        $query = $this->db->get();
        return ($object)?$query->result():$query->result_array();
    }

    public function update($table, $fields, $conditions){
        $this->db->where($conditions);
        $return = $this->db->update($this->prefix.$table, $fields);
        return $return;
    }

    public function insert($table, $fields){
        $return = $this->db->insert($this->prefix.$table, $fields);
        return $return;
    }

    public function query($sql){
        $result = $this->db->query($sql);
        if($result){
            return $result->result_array();
        }
        return false;
    }

    public function count($table, $fields = '*', $conditions = null){
        $this->db->select($fields);
        $this->db->from($this->prefix.$table);
        if(!is_null($conditions)){
            $this->db->where($conditions);
        }
        return $this->db->count_all_results();
    }

    public function delete($table, $conditions){
        return $this->db->delete($this->prefix.$table, $conditions);
    }

}
