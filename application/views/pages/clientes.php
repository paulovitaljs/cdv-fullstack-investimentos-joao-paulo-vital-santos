<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col d-flex justify-content-between">
                    <h2 class="page-title">
                        <?= $page_title ?>
                    </h2>
                    <?php if (!empty($clientes)) : ?>
                        <div>
                            <a type="button" class="btn btn-success novo-cliente" data-toggle="tooltip" data-placement="top" title="Novo Cliente">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user-plus" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <circle cx="9" cy="7" r="4"></circle>
                                    <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                                    <path d="M16 11h6m-3 -3v6"></path>
                                </svg>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <?php if (!empty($clientes)) : ?>
                <div class="row row-cards">
                    <div class="col-12">
                        <div class="card">
                            <div class="table-responsive p-3">
                                <table id="data-table" class="table table-striped table-bordered table-vcenter card-table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nome</td>
                                            <th>E-mail</td>
                                            <th>Ações</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($clientes as $cliente) : ?>
                                            <tr>
                                                <td><?= $cliente['nome'] ?></td>
                                                <td><?= $cliente['email'] ?></td>
                                                <td class="text-center">
                                                    <a type="button" class="btn btn-default edit-cliente" client-id="<?= $cliente['id'] ?>" data-toggle="tooltip" data-placement="top" title="Editar">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                            <path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3"></path>
                                                            <path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3"></path>
                                                            <line x1="16" y1="5" x2="19" y2="8"></line>
                                                        </svg>
                                                    </a>
                                                    <a type="button" class="btn btn-default remove-cliente" client-id="<?= $cliente['id'] ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                            <line x1="4" y1="7" x2="20" y2="7"></line>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                                        </svg>
                                                    </a>
                                                    <a type="button" class="btn btn-default portfolio-cliente" client-id="<?= $cliente['id'] ?>" client-name="<?= $cliente['nome'] ?>" data-toggle="tooltip" data-placement="top" title="Portfólio">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-list-check" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                            <path d="M3.5 5.5l1.5 1.5l2.5 -2.5"></path>
                                                            <path d="M3.5 11.5l1.5 1.5l2.5 -2.5"></path>
                                                            <path d="M3.5 17.5l1.5 1.5l2.5 -2.5"></path>
                                                            <line x1="11" y1="6" x2="20" y2="6"></line>
                                                            <line x1="11" y1="12" x2="20" y2="12"></line>
                                                            <line x1="11" y1="18" x2="20" y2="18"></line>
                                                        </svg>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <div class="row row-cards">
                    <div class="w-100">
                        <div class="card">
                            <did class="card-body d-flex justify-content-center">
                                <div class="text-center">
                                    <p>Ainda não há clientes cadastrados. Que tal cadastrar seu primeiro cliente?</p>
                                    <a type="button" class="btn btn-success novo-cliente">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user-plus" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <circle cx="9" cy="7" r="4"></circle>
                                            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                                            <path d="M16 11h6m-3 -3v6"></path>
                                        </svg> &nbsp; Cadastrar Cliente
                                    </a>
                                </div>
                            </did>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>

    <!-- Seção para as modais usadas na página -->

    <div class="modal modal-cliente" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Novo Cliente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="save-edit-cliente">
                        <input type="hidden" class="form-control" name="id">
                        <div class="form-group mb-3">
                            <label class="form-label">Nome</label>
                            <input required="required" type="text" class="form-control" name="nome" placeholder="Nome Completo">
                        </div>
                        <div class="form-group mb-3">
                            <label class="form-label">E-mail</label>
                            <input required="required" type="email" class="form-control" name="email" placeholder="E-mail">
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Informação Adicional</label>
                                    <textarea name="info-adicional" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default link-secondary" data-bs-dismiss="modal">
                        Cancelar
                    </a>
                    <a href="#" class="btn btn-primary ms-auto btn-salvar">
                        <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                        <!-- SVG icon code -->
                        Salvar
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-confirm-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-status bg-danger"></div>
                <div class="modal-body text-center">
                    <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                    <!-- SVG icon code with class="mb-2 text-danger icon-lg" -->
                    <h3>Confirme a operação</h3>
                    <div class="text-muted">Deseja realmente excluir o cliente <span class="nome-cliente"></span>?</div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <a href="#" class="btn btn-white" data-bs-dismiss="modal">Cancelar</a>
                    <a href="#" class="btn btn-danger btn-excluir" data-bs-dismiss="modal">Excluir</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-portfolio" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Portfólio de <span class="nome-cliente"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Forneça o período para análise do portfólio do cliente. Datas em branco exibem o portfólio atual.</p>
                    <form class="calculo-posicoes">
                        <input type="hidden" class="form-control" name="id">
                        <div class="row">
                            <div class="col-6 col-md-5 form-group d-flex justify-content-center align-items-center">
                                <label class="form-lable">De&nbsp;</label>
                                <input required="required" type="date" class="form-control" name="de" placeholder="Início">
                            </div>
                            <div class="col-6 col-md-5 form-group d-flex justify-content-center align-items-center">
                            <label class="form-lable">Até&nbsp;</label>
                                <input required="required" type="date" class="form-control" name="ate" placeholder="Fim">
                            </div>
                            <div class="col-12 col-md-2 form-group">
                                <button class="btn btn-primary w-100" type="submit">
                                    Calcular
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="div-portfolio mt-3 ">

                        <div class="card" style="overflow-y: scroll; height: 200px;">
                            <did class="card-body">
                                <div class="message text-center">Sem movimentações</div>
                                <table class="table-posicoes table table-striped table-bordered table-vcenter card-table">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Ativo</th>
                                            <th>Qtd</th>
                                            <th>Preço Médio</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default link-secondary" data-bs-dismiss="modal">
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>