<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Entrar</title>
    <!-- CSS files -->
    <link href="<?= base_url("assets/css/tabler.min.css") ?>" rel="stylesheet"/>
    <link rel="icon" href="<?= base_url("assets/img/cropped-CDV-Favicon-32x32.webp") ?>" sizes="32x32" />
    <link href="<?= base_url("assets/css/pnotify.brighttheme.css") ?>" rel="stylesheet"/>
    <link href="<?= base_url("assets/css/pnotify.buttons.css") ?>" rel="stylesheet"/>
    <link href="<?= base_url("assets/css/pnotify.css") ?>" rel="stylesheet"/>
  </head>
  <body class="antialiased">
  <div class="wrapper">
    <div class="page page-center">
      <div class="container-tight py-4">
        <div class="text-center mb-4">
          <a href="."><img src="<?= base_url("assets/img/CDV.png") ?>" height="36" alt=""></a>
        </div>
        <form class="card card-md login" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Identifique-se</h2>
            <div class="mb-3">
              <label class="form-label">E-mail</label>
              <input type="email" name="email" required="true" class="form-control" placeholder="Informe seu e-mail">
            </div>
            <div class="mb-2">
              <label class="form-label">
                Senha
              </label>
              <div class="input-group input-group-flat">
                <input type="password" name="senha" required="true" class="form-control"  placeholder="Senha"  autocomplete="off">
              </div>
            </div>
            <div class="form-footer">
              <button type="submit" class="btn btn-primary w-100">Entrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script>
      var base_url = '<?= base_url() ?>';
    </script>
    <script src="<?= base_url("assets/js/tabler.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/jquery-3.6.0.min.js") ?>"></script>
    <script src="<?= base_url("assets/js/pnotify.js") ?>"></script>
    <script src="<?= base_url("assets/js/pnotify.buttons.js") ?>"></script>
    <script src="<?= base_url("assets/js/main.js") ?>"></script>
  </body>
</html>