<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col d-flex justify-content-between">
                    <h2 class="page-title">
                        <?= $page_title ?>
                    </h2>
                    <?php if (!empty($lotes)): ?>
                    <div>
                        <a type="button" class="btn btn-default refresh" data-toggle="tooltip" data-placement="top" title="Recarregar Página">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-refresh" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M20 11a8.1 8.1 0 0 0 -15.5 -2m-.5 -4v4h4"></path>
                                <path d="M4 13a8.1 8.1 0 0 0 15.5 2m.5 4v-4h-4"></path>
                            </svg>
                        </a>
                        <a type="button" class="btn btn-success novo-lote" data-toggle="tooltip" data-placement="top" title="Importar Lote de Movimentações">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-file-import" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M14 3v4a1 1 0 0 0 1 1h4"></path>
                                <path d="M5 13v-8a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2h-5.5m-9.5 -2h7m-3 -3l3 3l-3 3"></path>
                            </svg>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <?php if (!empty($lotes)): ?>
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="table-responsive p-3">
                            <table id="data-table" class="table table-striped table-bordered table-vcenter card-table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Data</td>
                                        <th class="text-center">Tamanho</td>
                                        <th class="text-center">Status</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lotes as $lote): ?>
                                            <tr>
                                                <td><?= $lote['filename'] ?></td>
                                                <td><?= date('d/m/Y H:m:s', strtotime($lote['data'])) ?></td>
                                                <td class="text-center"><?= $lote['tamanho'] ?> Kb</td>
                                                <td class="text-center">
                                                <?php 
                                                    switch($lote['status']){
                                                        case '1':
                                                            echo '<span class="badge bg-dark-lt">Em fila</span>';
                                                            break;
                                                        case '2':
                                                            echo '<span class="badge bg-azure text-white">Em processamento</span>';
                                                            break;
                                                        case '3':
                                                            echo '<span class="badge bg-green text-white">Sucesso</span>';
                                                            break;
                                                        default:
                                                            echo '<span class="badge bg-red text-white">Erro</span>';

                                                    }
                                                ?>
                                                </td>
                                            </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php else: ?>
                <div class="row row-cards">
                    <div class="w-100">
                        <div class="card">
                            <did class="card-body d-flex justify-content-center">
                                <div class="text-center">
                                    <p>Aqui ficarão as informações sobre os arquivos de cadastro de movimentações em lote. Envie o primeiro arquivo!</p>
                                    <a type="button" class="btn btn-success novo-lote">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-file-import" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <path d="M14 3v4a1 1 0 0 0 1 1h4"></path>
                                            <path d="M5 13v-8a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2h-5.5m-9.5 -2h7m-3 -3l3 3l-3 3"></path>
                                        </svg> &nbsp; Enviar Lote de Movimentações
                                    </a>
                                </div>
                            </did>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>

    <!-- Seção para as modais usadas na página -->

    <div class="modal modal-upload-lote" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Envio de arquivo de lote</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div>
                        <p>Forneça um arquivo texto com a extensão .csv de acordo com o exemplo abaixo. Depois clique em enviar. <span class="text-orange">Arquivos com formatos diferentes serão ignorados</span>.</p>
                        
                        <!-- O texto dentro da tag <pre> deve permanecer sem identação -->
                        <pre>Data,Cliente,Ativo,Tipo,Quantidade,Valor Unitário
01/07/20,Maria da Silva,IVVB11,Compra,30,"240,59"
03/07/20,Maria da Silva,IVVB11,Venda,17,"241,2"
15/08/20,Maria da Silva,IVVB11,Compra,12,"238,67"
19/09/20,Maria da Silva,IVVB11,Compra,35,"236,8"
</pre>
                    </div>
                    <form class="upload-lote">
                        <input required="required" type="file" class="form-control" name="lote">
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default link-secondary" data-bs-dismiss="modal">
                        Cancelar
                    </a>
                    <a href="#" class="btn btn-primary ms-auto btn-enviar-arquivo">
                        <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                        <!-- SVG icon code -->
                        Enviar
                    </a>
                </div>
            </div>
        </div>
    </div>