<div class="page-wrapper">
    <div class="container-xl">
        <!-- Page title -->
        <div class="page-header d-print-none">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        <?= $page_title ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <?php if (!empty($movimentacoes)): ?>
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="table-responsive p-3">
                            <table id="data-table" class="table table-striped table-bordered table-vcenter card-table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Data</td>
                                        <th>Cliente</td>
                                        <th class="text-center">Ativo</td>
                                        <th class="text-center">Tipo</td>
                                        <th class="text-center">Qtd</td>
                                        <th class="text-center">Valor Unit</td>
                                        <th class="text-center">Ações</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($movimentacoes as $movimentacao) : ?>
                                            <tr>
                                                <td><?= date('d/m/Y', strtotime($movimentacao['data'])) ?></td>
                                                <td><?= $movimentacao['nome'] ?></td>
                                                <td class="text-center"><?= $movimentacao['ativo'] ?></td>
                                                <td class="text-center"><?= ($movimentacao['tipo'] === '1') ? 'Compra' : 'Venda' ?></td>
                                                <td class="text-center"><?= $movimentacao['qtd'] ?></td>
                                                <td class="text-center"><?= $movimentacao['valor'] ?></td>
                                                <td class="text-center">
                                                    <a type="button" class="btn btn-default remove-movimentacao" movimentacao-id="<?= $movimentacao['id'] ?>" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                            <line x1="4" y1="7" x2="20" y2="7"></line>
                                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                                        </svg>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php else: ?>
                <div class="row row-cards">
                    <div class="w-100">
                        <div class="card">
                            <did class="card-body d-flex justify-content-center">
                                <p>Você ainda não tem movimentações cadastradas. Que <a href="<?= base_url('lotes') ?>">tal enviar seu primeiro arquivo de cadastro em lote</a>?</p>
                            </did>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>

    <!-- Seção para as modais usadas na página -->

    <div class="modal modal-confirm-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-status bg-danger"></div>
                <div class="modal-body text-center">
                    <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                    <!-- SVG icon code with class="mb-2 text-danger icon-lg" -->
                    <h3>Confirme a operação</h3>
                    <div class="text-muted">Deseja realmente excluir esta movimentação?</div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <a href="#" class="btn btn-white" data-bs-dismiss="modal">Cancelar</a>
                    <a href="#" class="btn btn-danger btn-excluir-movimentacao" data-bs-dismiss="modal">Excluir</a>
                </div>
            </div>
        </div>
    </div>