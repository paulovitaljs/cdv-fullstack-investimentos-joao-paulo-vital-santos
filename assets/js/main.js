$(document).ready(function () {

    //Para destacar a opção de menu corrente
    let current_url = window.location.href;
    $('.nav-item').removeClass('active');
    $(`a[current-url='${current_url}']`).parent().addClass('active');

    //Para acionar a operação de login
    $('form.login').submit(function (e){
        e.preventDefault();
        
        request(`${base_url}login/entrar`, $(this).serialize(), function(response){
            $("button[type='submit']").text('Aguarde ...')
            window.location.href = `${base_url}clientes`;
        })
    });

    //Acionar a operação de logout
    $('.logout').click(function(){
        request(`${base_url}login/sair`, null, function(){
            window.location.reload();
        });
    })

    if($('#data-table').length > 0){

        //Configura o plugin datatable
        $('#data-table').DataTable({
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/pt_br.json'
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    text: `<a><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-printer" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">   <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>   <path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2"></path>   <path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4"></path>   <rect x="7" y="13" width="10" height="8" rx="2"></rect></svg> Imprimir`,
                    className: "btn btn-default  mb-2"
                },
                {
                    extend: 'excel',
                    text: `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-table-export" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">   <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>   <path d="M11.5 20h-5.5a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v7.5m-16 -3.5h16m-10 -6v16m4 -1h7m-3 -3l3 3l-3 3"></path></svg> Excel`,
                    className: "btn btn-default  mb-2"
                },
                {
                    extend: 'copy',
                    text: `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-clipboard" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">   <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>   <path d="M9 5h-2a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-12a2 2 0 0 0 -2 -2h-2"></path>   <rect x="9" y="3" width="6" height="4" rx="2"></rect></svg> Copiar`,
                    className: "btn btn-default  mb-2"
                },
                {
                    extend: 'pdf',
                    text: `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-file-download" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">   <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>   <path d="M14 3v4a1 1 0 0 0 1 1h4"></path>   <path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z"></path>   <line x1="12" y1="11" x2="12" y2="17"></line>   <polyline points="9 14 12 17 15 14"></polyline></svg> PDF`,
                    className: "btn btn-default mb-2"
                },
            ],
            pageLength: 25
        });
    }

    //Aciona o plugin tootltip disponível no bootstrap
    $("[data-toggle='tooltip']").tooltip();

    /////// VIEW DOS CLIENTES ///////

    //Aciona a modal para castro de novo cliente
    $('.novo-cliente').click(function(){
        $('form.save-edit-cliente .form-control').each(function(element) {
            $(this).val(""); //Limpa os campos do formulário a cada exibição da modal
        });
        $('.modal-cliente').modal('show');
    });

    $('form.save-edit-cliente').submit(function(e){
        e.preventDefault();

        request(`${base_url}clientes/save`, $(this).serialize(), function(response){
            $('form.save-edit-cliente .form-control').each(function(element) {
                $(this).val("");
            });
            $('.modal-cliente').modal('hide');
            window.location.reload();
        });
        
    });

    $('.btn-salvar').click(function(e){
        e.preventDefault();

        execPromise(function(){
            $('form.save-edit-cliente').find(".form-control[required]").each(function(){
                if($(this).val() === ''){
                    $(this).addClass('is-invalid');
                }else{
                    $(this).removeClass('is-invalid');
                }
            });
        }).then(e => {
            if($('.is-invalid').length == 0){
                $('form.save-edit-cliente').submit();
            }
        });
    });

    $('.edit-cliente').click(function(){
        let client_id = $(this).attr('client-id');

        request(`${base_url}clientes/get/${client_id}`, null, function(response){
            $("input[name='id']").val(response.data.id);
            $("input[name='nome']").val(response.data.nome);
            $("input[name='email']").val(response.data.email);
            $("textarea[name='info-adicional']").val(response.data['info-adicional']);
            $('.modal-cliente').modal('show');
        }, null, 'GET');
    });
    
    //Solicita confirmação para exclusão de cliente
    $('.remove-cliente').click(function(){
        let client_id = $(this).attr('client-id');
        $('.btn-excluir').attr('client-id', client_id);
        $('.modal-confirm-delete').modal('show');
    });

    //Aciona a operação de exclusão de um cliente
    $('.btn-excluir').click(function(e){
        e.preventDefault();

        //obtém o id do cliente para uso na solicitação
        let client_id = $(this).attr('client-id');

        request(`${base_url}clientes/delete/${client_id}`, null, function(response){
            window.location.reload();
        }, null);

    });

    //Para exibir a modal com as posições de um cliente
    $('.portfolio-cliente').click(function(e){
        e.preventDefault();

        let client_id = $(this).attr('client-id');
        let client_name = $(this).attr('client-name');

        //identifica o cliente na modal do portfólio
        $('span.nome-cliente').text(client_name);
        $("form.calculo-posicoes input[name='id']").val(client_id);

        //Pede ao servidor as posições atuais do cliente
        request(`${base_url}clientes/posicoes/${client_id}`, null, function(response){
            $('div.message').hide();
            $('table.table-posicoes tbody tr').remove();
            $('table.table-posicoes').show();

            //Exibe as posições na tabela
            for(let [key, value] of Object.entries(response.posicoes)){
                if(value['qtd']){
                    $('table.table-posicoes tbody').append(
                        `<tr>
                          <td>${value['ativo']}</td>
                          <td>${value['qtd']}</td>
                          <td>${(value['acum']/value['total']).toFixed(2).replace('.', ',')}</td>
                         </tr>`
                    );
                }
            }
            if(jQuery.isEmptyObject(response.posicoes)){
                $('table.table-posicoes').hide();
                $('div.message').show();
            }
        }, null, 'POST');

        $('.modal-portfolio').modal('show');
    });

    //Para submeter solicitações de cálculo das posições com base em um período de datas
    $('form.calculo-posicoes').submit(function(e){
        e.preventDefault();
        let client_id = $('form.calculo-posicoes').find("input[name='id']").val();
        //Pede ao servidor as posições atuais do cliente
        request(`${base_url}clientes/posicoes/${client_id}`, $(this).serialize(), function(response){
            //Exibe as posições na tabela
            $('div.message').hide();
            $('table.table-posicoes tbody tr').remove();
            $('table.table-posicoes').show();

            //Exibe as posições na tabela
            for(let [key, value] of Object.entries(response.posicoes)){
                if(value['qtd']){
                    $('table.table-posicoes tbody').append(
                        `<tr>
                          <td>${value['ativo']}</td>
                          <td>${value['qtd']}</td>
                          <td>${(value['acum']/value['total']).toFixed(2).replace('.', ',')}</td>
                         </tr>`
                    );
                }
            }
            if(jQuery.isEmptyObject(response.posicoes)){
                $('table.table-posicoes').hide();
                $('div.message').show();
            }
        }, null, 'POST');
    });

    /////// VIEW DAS MOVIMENTAÇÕES ///////

    //Solcita confirmação da operação de exclusão
    $('.remove-movimentacao').click(function(){
        let movimentacao_id = $(this).attr('movimentacao-id');
        $('.btn-excluir-movimentacao').attr('movimentacao-id', movimentacao_id);
        $('.modal-confirm-delete').modal('show');
    });

    //Aciona a exclusão de uma movimentação
    $('.btn-excluir-movimentacao').click(function(e){
        e.preventDefault();

        //obtém o id da movimentação para uso na solicitação
        let movimentacao_id = $(this).attr('movimentacao-id');

        request(`${base_url}movimentacoes/delete/${movimentacao_id}`, null, function(response){
            window.location.reload();
        }, null);

    });

    /////// VIEW DOS ARQUIVOS DE LOTE ///////

    //Exibe a modal para uplod de arquivos de lote
    $('.novo-lote').click(function(){
        $('.modal-upload-lote').modal('show');
    });

    //Valida se o arquivo de lote foi selecionado
    $('.btn-enviar-arquivo').click(function(e){
       execPromise(function(){
            $('form.upload-lote').find(".form-control[required]").each(function(){
                if($(this).val() === ''){
                    $(this).addClass('is-invalid');
                }else{
                    $(this).removeClass('is-invalid');
                }
            });
        }).then(e => {
            if($('.is-invalid').length == 0){
                $('form.upload-lote').submit();
            }
        });
    });

    //Envia o arquivo de lote
    $('form.upload-lote').submit(function(e){
        e.preventDefault();
        let formElement = document.querySelector("form.upload-lote");
        request(base_url+"lotes/upload", new FormData(formElement), function(response){
            window.location.reload();
        }, null, 'POST', true);
    });

    //Ação do botão refresh
    $('.refresh').click(function(){
        window.location.reload();
    });
});

/**
 * Esta função excuta uma requisição ajax.
 * @param {*} url a url do recurso
 * @param {*} data um objeto contendo os dados a serem enviados
 * @param {*} _success uma callback a ser chamada em caso de sucesso
 * @param {*} _error uma callback a ser chamada em caso de erro
 * @param {*} type o tipo da requisição (POST, GET, PUT, DELETE etc)
 * @param {*} files true caso haja upload de arquivo, false caso contrário
 */
function request(url, data, _success = function() {}, _error = function(){}, type = 'POST', files = false){
    var _objRequest = {
        url: url,
        type: type,
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            if (data.success) {
                _success(data);

            } else if(data.error){
                new PNotify({
                    title: 'Oops!',
                    text: data.error,
                    type: 'error'
                });

                _error(data);

            }else{
                new PNotify({
                    title: 'Oops!',
                    text: "Por alguma razão sua requisição não foi processada com sucesso. Tente novamente em instantes.",
                    type: 'error'
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            new PNotify({
                title: 'Oops!',
                text: "Por alguma razão sua requisição não foi processada com sucesso. Tente novamente em instantes.",
                type: 'error'
            });
        }
    };

    if (files === true) {
        _objRequest.processData = false;
        _objRequest.contentType = false;
    }

    $.ajax(_objRequest);
}

/**
 * Esta função foi necessária para 'forçar' a ordem de execução de algumas callbacks usadas para manipular o DOM.
 * @param {*} exec 
 * @returns 
 */
function execPromise(exec){
    return new Promise((resolve, reject) => {
        try{
            resolve(exec())
        }catch(error){
            reject(error);
        }
    })
}